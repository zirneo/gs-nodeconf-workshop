package loadtests

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.util.Random

class ViewCacheServiceSimulation extends Simulation {

  val url = "http://34.66.241.35/get-data?key=1234"
  val randomNumGen = new Random()
  val scn: ScenarioBuilder = scenario("view-cache-simulation")
    .exec(http("view-cache-simulation")
      .get(url)
      .queryParam("key", _ => randomNumGen.nextInt(500).toString)
      .check(status is 200)
    )

  setUp(
    scn.inject(
      rampUsersPerSec (0) to 100 during (10 seconds),
      constantUsersPerSec(100) during (1 minute)
    )
  )
    .assertions(forAll.successfulRequests.percent.gte(99.9))
    .assertions(forAll.requestsPerSec.between(85, 500))
}
