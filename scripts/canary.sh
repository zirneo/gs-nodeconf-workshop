#!/bin/sh
APP=${1:?Must specifify a app path}
VERSION=v${2:?Must specicify a version}
NAMESPACE=${3:?Must specify a namespace}
WEIGHT=${4:?Must specifify a weight}

sed -i "s|VERSION_LABEL|${VERSION}|g" ${APP}-deployment.yaml

if [ 100 -eq ${WEIGHT} ]; then
    kubectl delete deployment -l app=${APP} --ignore-not-found --namespace ${NAMESPACE}
    yq '.' destination-rule.yaml | jq --arg version ${VERSION} 'setpath(["spec","subsets",0];{name:"active",labels:{version:$version}})' | yq . -y > ${APP}-destination-rule.yaml
    yq '.' virtual-service.yaml | jq --arg host "${APP}-svc" 'setpath(["spec","http",0,"route",0];{destination:{host:$host,subset:"active"},weight:100})' | yq . -y > ${APP}-virtual-service.yaml
else
    ACTIVE_VERSION=$(kubectl get deployment ${APP}-active --namespace ${NAMESPACE} --ignore-not-found -o json | jq '.metadata.labels.version' -r)
    if [ "${ACTIVE_VERSION}" ] && [ "${ACTIVE_VERSION}" != "${VERSION}" ]; then

        SUBSETS=$(kubectl get destinationrules.networking.istio.io ${APP} -o json --namespace ${NAMESPACE} | jq '.spec.subsets | map(select(.name != "canary"))' | jq --arg version ${VERSION} '. + [{name:"canary",labels:{version:$version}}]')
        yq '.' destination-rule.yaml | jq --argjson subsets "${SUBSETS}" 'setpath(["spec","subsets"];$subsets)' | yq . -y > ${APP}-destination-rule.yaml

        ROUTES=$(kubectl get virtualservices.networking.istio.io ${APP} -o json --namespace ${NAMESPACE} | jq '.spec.http' | jq --arg host "${APP}-svc" --argjson weight ${WEIGHT} 'setpath([0,"route",1];{destination:{host:$host,subset:"canary"},weight:$weight})')
        ROUTES=$(echo ${ROUTES}| jq --argjson weight $((100-${WEIGHT})) 'setpath([0,"route",0,"weight"];$weight)')
        yq '.' virtual-service.yaml | jq --argjson route "${ROUTES}" 'setpath(["spec","http"];$route)' | yq . -y > ${APP}-virtual-service.yaml

        sed -i "s|-active|-canary|" ${APP}-deployment.yaml
        kubectl delete deployment ${APP}-canary --ignore-not-found --namespace ${NAMESPACE}
    else
        CANARY_VERSION=$(kubectl get deployment ${APP}-canary --namespace ${NAMESPACE} --ignore-not-found -o json | jq '.metadata.labels.version' -r)
        if [ "${CANARY_VERSION}" ]; then
            kubectl delete deployment ${APP}-canary --ignore-not-found --namespace ${NAMESPACE}
            kubectl delete deployment ${APP}-active --ignore-not-found --namespace ${NAMESPACE}

            sed -i "s|-active|-canary|" ${APP}-deployment.yaml
            kubectl apply -f ${APP}-deployment.yaml --namespace ${NAMESPACE}

            sed -i "s|${VERSION:1}|${CANARY_VERSION:1}|g" ${APP}-deployment.yaml
            sed -i "s|-canary|-active|" ${APP}-deployment.yaml

            SUBSETS=$(kubectl get destinationrules.networking.istio.io ${APP} -o json --namespace ${NAMESPACE} | jq '.spec.subsets')
            SUBSETS=$(echo ${SUBSETS} | sed 's/canary/aux/' | sed 's/active/canary/' | sed 's/aux/active/')
            yq '.' destination-rule.yaml | jq --argjson subsets "${SUBSETS}" 'setpath(["spec","subsets"];$subsets)' | yq . -y > ${APP}-destination-rule.yaml

            ROUTES=$(kubectl get virtualservices.networking.istio.io ${APP} -o json --namespace ${NAMESPACE} | jq '.spec.http')
            yq '.' virtual-service.yaml | jq --argjson route "${ROUTES}" 'setpath(["spec","http"];$route)' | yq . -y > ${APP}-virtual-service.yaml

        else
            kubectl delete deployment -l app=${APP} --ignore-not-found --namespace ${NAMESPACE}
            yq '.' destination-rule.yaml | jq --arg version ${VERSION} 'setpath(["spec","subsets",0];{name:"active",labels:{version:$version}})' | yq . -y > ${APP}-destination-rule.yaml
            yq '.' virtual-service.yaml | jq --arg host "${APP}-svc" 'setpath(["spec","http",0,"route",0];{destination:{host:$host,subset:"active"},weight:100})' | yq . -y > ${APP}-virtual-service.yaml
        fi
    fi
fi

rm destination-rule.yaml
rm virtual-service.yaml
mkdir k8s
mv *.yaml k8s